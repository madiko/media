Medienvorlagen für Ortsgruppen
=============================

Dieses Repo umfasst Medienvorlagen für lokale Ortsgruppen, auf Basis des offiziellen Corporate Designs von https://bits-und-baeume.org/material/de


Beispiele:
----------------

* Mastodon Profilbild:

![](B&B_Logo_RGB-Berlin-300px-white-bg.png)

* Mastodon Header:

![](B&B_Header-Mastodon.png)


Tipps:
---------
- Die Mediendateien im Browser zu durchsuchen, dauert Zeit. Schneller: Repo lokal clonen und dann mithilfe der Bildervorschau die passende Datei heraussuchen.
- Für das Editieren der Vektorgraphiken bietet sich die Software Inkscape an.
